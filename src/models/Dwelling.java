package models;

import java.math.BigInteger;

public class Dwelling {
    private BigInteger id;
    private BigInteger VHE;
    private String streetName;
    private BigInteger houseNumber;
    private String city;
    private String country;
    private double price;

    public Dwelling(BigInteger id, BigInteger VHE, String streetName, BigInteger houseNumber, String city, String country, double price) {
        this.id = id;
        this.VHE = VHE;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.city = city;
        this.country = country;
        this.price = price;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getVHE() {
        return VHE;
    }

    public void setVHE(BigInteger VHE) {
        this.VHE = VHE;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public BigInteger getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(BigInteger houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
