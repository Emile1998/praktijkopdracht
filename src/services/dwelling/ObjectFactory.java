
package services.dwelling;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the services.dwelling package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Createdwellingrequest_QNAME = new QName("http://schemas.hu.fnt.nl/berichten/request", "createdwellingrequest");
    private final static QName _Faulthandling_QNAME = new QName("http://schemas.hu.fnt.nl/berichten/fault", "faulthandling");
    private final static QName _Getdwellingresponse_QNAME = new QName("http://schemas.hu.fnt.nl/berichten/response", "getdwellingresponse");
    private final static QName _Getdwellingrequest_QNAME = new QName("http://schemas.hu.fnt.nl/berichten/request", "getdwellingrequest");
    private final static QName _Createdwellingresponse_QNAME = new QName("http://schemas.hu.fnt.nl/berichten/response", "createdwellingresponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: services.dwelling
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Getrequest }
     * 
     */
    public Getrequest createGetrequest() {
        return new Getrequest();
    }

    /**
     * Create an instance of {@link Createrequest }
     * 
     */
    public Createrequest createCreaterequest() {
        return new Createrequest();
    }

    /**
     * Create an instance of {@link Createresponse }
     * 
     */
    public Createresponse createCreateresponse() {
        return new Createresponse();
    }

    /**
     * Create an instance of {@link Getresponse }
     * 
     */
    public Getresponse createGetresponse() {
        return new Getresponse();
    }

    /**
     * Create an instance of {@link Fault }
     * 
     */
    public Fault createFault() {
        return new Fault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Createrequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hu.fnt.nl/berichten/request", name = "createdwellingrequest")
    public JAXBElement<Createrequest> createCreatedwellingrequest(Createrequest value) {
        return new JAXBElement<Createrequest>(_Createdwellingrequest_QNAME, Createrequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Fault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hu.fnt.nl/berichten/fault", name = "faulthandling")
    public JAXBElement<Fault> createFaulthandling(Fault value) {
        return new JAXBElement<Fault>(_Faulthandling_QNAME, Fault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getresponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hu.fnt.nl/berichten/response", name = "getdwellingresponse")
    public JAXBElement<Getresponse> createGetdwellingresponse(Getresponse value) {
        return new JAXBElement<Getresponse>(_Getdwellingresponse_QNAME, Getresponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Getrequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hu.fnt.nl/berichten/request", name = "getdwellingrequest")
    public JAXBElement<Getrequest> createGetdwellingrequest(Getrequest value) {
        return new JAXBElement<Getrequest>(_Getdwellingrequest_QNAME, Getrequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Createresponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hu.fnt.nl/berichten/response", name = "createdwellingresponse")
    public JAXBElement<Createresponse> createCreatedwellingresponse(Createresponse value) {
        return new JAXBElement<Createresponse>(_Createdwellingresponse_QNAME, Createresponse.class, null, value);
    }

}
