package services.dwelling;

import models.Dwelling;

import javax.jws.WebService;
import java.math.BigInteger;
import java.util.ArrayList;

@WebService( endpointInterface= "services.dwelling.WSInterface")
public class DwellingImpl implements WSInterface {

    private ArrayList<Dwelling> dwellings = new ArrayList<>();

    @Override
    public Createresponse createDwelling(Createrequest createrequest) {
        ObjectFactory factory = new ObjectFactory();
        Dwelling dwelling = this.createDwellingObject(createrequest);

        Createresponse response = factory.createCreateresponse();

        BigInteger actualResult;
        if(saveDwelling(dwelling)) {
            actualResult = dwelling.getId();
        } else {
            actualResult = null;
        }
        response.setResult(actualResult);
        return response;
    }

    @Override
    public Getresponse getDwelling(Getrequest getrequest) throws Fault_Exception {
        ObjectFactory factory = new ObjectFactory();
        Getresponse response = factory.createGetresponse();
        for (Dwelling d : dwellings) {
            if(d.getId().equals(getrequest.getId())) {
                return this.createDwellingResponseObject(d, response);
            }
        }
        throw new Fault_Exception("No dwelling found with this dwellingId: " + getrequest.getId(),null);
    }

    private Dwelling createDwellingObject(Createrequest request) {
        BigInteger id = request.getId();
        BigInteger VHE = request.getVHE();
        String streetName = request.getStreetName();
        BigInteger houseNumber = request.getHouseNumber();
        String city = request.getCity();
        String country = request.getCountry();
        double price = request.getPrice();

        return new Dwelling(id, VHE, streetName, houseNumber, city, country, price);
    }

    private Getresponse createDwellingResponseObject(Dwelling dwelling, Getresponse getresponse) {
        getresponse.setId(dwelling.getId());
        getresponse.setVHE(dwelling.getVHE());
        getresponse.setStreetName(dwelling.getStreetName());
        getresponse.setHouseNumber(dwelling.getHouseNumber());
        getresponse.setCity(dwelling.getCity());
        getresponse.setCountry(dwelling.getCountry());
        getresponse.setPrice(dwelling.getPrice());

        return getresponse;
    }

    private boolean saveDwelling(Dwelling dwelling) {
        for (Dwelling d : dwellings) {
            if(d.getId().equals(dwelling.getId())) {
                return false;
            }
        }
        this.dwellings.add(dwelling);
        return true;
    }
}
